
-- SUMMARY --
Module Views Scroll Pager provides extended pager for Views:
pager with horizontal scroll. Module is friendly for AJAX
and multiple pagers processing.

-- REQUIREMENTS --
Module depends on Views, Libraries (2.x) and jScrollPane (Javascript library).
Module requires IE8 and higher.

-- INSTALLATION --
1. Extract module files to  /sites/all/modules folder.
2. Ensure that Views and Libraries (2.x) have been already installed.
3. Download jScrollPane library - http://jscrollpane.kelvinluck.com/#download.
4. Put files jquery.jscrollpane.min.js and jquery.jscrollpane.css
   to /sites/all/libraries/jscrollpane folder.
5. Enable module Views Scroll Pager.

For more information about module installation see INSTALL.txt.

-- CONFIGURATION --
1. Open existing or create new View.
2. Select "Horizontal scroll pager" in Pager options.
3. Enjoy!

-- CUSTOMIZATION --
Module is provided with default CSS styles, which can be overridden
through your custom CSS.

-- CONTACT AND CREDITS --
Module was developed by Evgeniy Melnikov:
    - http://www.angarsky.ru/
    - http://drupal.org/user/1683348
